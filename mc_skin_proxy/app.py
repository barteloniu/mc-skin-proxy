import socket
from select import select
from .proxy_connection import ProxyConnection


PROXY_HOST="0.0.0.0"
PROXY_PORT=6969
SERVER_ADDR="localhost"
SERVER_PORT=25565


threads = []


def run():
    # create a server socket
    proxy_server_sock = socket.socket()
    proxy_server_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    proxy_server_sock.bind((PROXY_HOST, PROXY_PORT))
    proxy_server_sock.listen()
    proxy_server_sock.setblocking(False)

    print(f"Proxy started on {PROXY_HOST}:{PROXY_PORT}")

    try:
        while True:
            select([proxy_server_sock], [], [])  # wait for a new connection
            # start a new proxy connection in a separate thread
            t = ProxyConnection(proxy_server_sock, SERVER_ADDR, SERVER_PORT)
            threads.append(t)
            t.start()
    except KeyboardInterrupt:
        for t in threads:
            t.killed = True
        print("Exiting")

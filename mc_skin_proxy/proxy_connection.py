import threading
from select import select
import socket
from .packet_reader import PacketReader


class ProxyConnection(threading.Thread):

    def __init__(self, sock, server_addr, server_port):
        super().__init__()
        self.killed = False

        self.client_sock, _ = sock.accept()
        self.client_sock.setblocking(False)

        self.server_sock = socket.socket()
        self.server_sock.connect((server_addr, server_port))
        self.server_sock.setblocking(False)

        # clientbound
        self.cb_reader = PacketReader(self.server_sock, self.client_sock)

        print(f"Accepted {self.client_sock.fileno()}")

    def run(self):
        while not self.killed:
            # get sockets awaiting a read
            readable = select([self.client_sock, self.server_sock], [], [])[0]
            for sock in readable:
                if sock.fileno() == self.client_sock.fileno():
                    try:
                        data = sock.recv(1024)
                    except ConnectionResetError:
                        print(f"Connection {self.client_sock.fileno()} reset by peer")
                        self.close()
                        return
                
                    # no data means the socket wants to disconnect
                    if not data:
                        self.close()
                        return

                    # wait for the socket to be writable
                    select([], [self.server_sock], [])
                    self.server_sock.send(data)
                else:
                    try:
                        self.cb_reader.recv()
                    except EOFError:
                        # no data means the socket wants to disconnect
                        self.close()
                        return

    def close(self):
        client_fd = self.client_sock.fileno()

        self.client_sock.close()
        self.server_sock.close()

        self.killed = True

        print(f"Closed {client_fd}")

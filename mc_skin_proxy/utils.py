import socket
import struct
import uuid


# mostly from https://github.com/ammaraskar/pyCraft/
# by Ammar Askar under the Apache license 2.0


def get_input_method(source):
    """ check if the passed object has a recv or read method """
    return source.recv if type(source) is socket.socket else source.read


def get_output_method(source):
    """ check if the passed object has a send or write method """
    return source.send if type(source) is socket.socket else source.write


class Varint():
    max_bytes = 5

    @classmethod
    def read(cls, source):
        get_data = get_input_method(source)

        number = 0
        byte_count = 0

        while True:
            byte = get_data(1)
            if len(byte) < 1:
                raise EOFError("Unexpected end of varint")
            
            byte = ord(byte)
            number |= (byte & 0x7f) << 7 * byte_count

            if not byte & 0x80:
                break

            byte_count += 1
            if byte_count > cls.max_bytes:
                raise ValueError("Tried reading a too long varint")

        return number

    @staticmethod
    def write(value, dest):
        put_data = get_output_method(dest)

        out = b""

        while True:
            byte = value & 0x7f
            value >>= 7
            out += struct.pack("B", byte | (0x80 if value > 0 else 0))
            if value == 0:
                break
        
        put_data(out)


class String():
    
    @staticmethod
    def read(source):
        get_data = get_input_method(source)

        length = Varint.read(source)
        return get_data(length).decode("utf-8")

    @staticmethod
    def write(value, dest):
        put_data = get_output_method(dest)

        value = value.encode("utf-8")
        Varint.write(len(value), dest)
        put_data(value)

class UUID():

    @staticmethod
    def read(source):
        get_data = get_input_method(source)

        return str(uuid.UUID(bytes=get_data(16)))

    @staticmethod
    def write(value, dest):
        put_data = get_output_method(dest)
        
        put_data(uuid.UUID(value).bytes)

from select import select
from io import BytesIO
from .utils import Varint, String, UUID


play_packets = {}
login_packets = {}


def login_packet(pid):
    """ decorator used to mark a function as a login packet handler """
    def decorator(func):
        login_packets[pid] = func
        return func
    return decorator


def play_packet(pid):
    """ decorator used to mark a function as a play packet handler """
    def decorator(func):
        play_packets[pid] = func
        return func
    return decorator


@login_packet(0x02)
def handle_login_success(data, packet_reader):
    packet_reader.playing = True


@login_packet(0x03)
def set_compression(data, packet_reader):
    packet_reader.compression = Varint.read(data)


@play_packet(0x32)
def player_info(data, packet_reader):
    action = Varint.read(data)
    if action == 0:
        # add player

        new_data = BytesIO()
        Varint.write(0x32, new_data)  # packet id
        Varint.write(action, new_data)

        players_len = Varint.read(data)
        Varint.write(players_len, new_data)

        for i in range(players_len):
            uuid = UUID.read(data)
            UUID.write(uuid, new_data)

            name = String.read(data)
            String.write(name, new_data)

            prop_count = Varint.read(data)
            if prop_count != 0:
                raise ValueError("Property array not empty")
            
            txt = packet_reader.texture_manager.get(name)
            if txt is None:
                # player doesn't have a skin
                Varint.write(0, new_data)  # property count
            else:
                Varint.write(1, new_data)  # property count
                String.write("textures", new_data)
                String.write(txt["value"], new_data)
                Varint.write(1, new_data)  # has signature
                String.write(txt["signature"], new_data)

            gamemode = Varint.read(data)
            Varint.write(gamemode, new_data)

            ping = Varint.read(data)
            Varint.write(ping, new_data)

            has_display_name = Varint.read(data)
            Varint.write(has_display_name, new_data)

            if has_display_name:
                display_name = String.read(data)
                String.write(display_name, new_data)
        
        # wait for the socket to be writable
        select([], [packet_reader.dest], [])

        # additional byte for uncompressed length
        Varint.write(new_data.tell() + 1, packet_reader.dest)

        # uncompressed length, zero means no compression
        Varint.write(0, packet_reader.dest)

        new_data.seek(0)
        packet_reader.dest.send(new_data.read())

        # prepare for the next packet
        packet_reader.length = -1
        packet_reader.buffer = BytesIO()

        return True  # do not send again


def parse(data, packet_reader):
    """ detect packet id and handle it """
    pid = Varint.read(data)

    if packet_reader.playing:
        flag = False  # whether modified data has been changed
        if pid in play_packets:
            flag = play_packets[pid](data, packet_reader)
        if not flag:
            packet_reader.send()
    else:
        if pid in login_packets:
            login_packets[pid](data, packet_reader)
        packet_reader.send()

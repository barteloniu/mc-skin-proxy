import requests


class TextureManager():
    name_to_uuid_url = "https://api.mojang.com/profiles/minecraft"
    uuid_to_texture_url = "https://sessionserver.mojang.com/session/minecraft/profile/{}?unsigned=false"

    def __init__(self):
        self.cache = {}

    def get(self, name):
        if name in self.cache:
            return self.cache[name]

        res = requests.post(self.name_to_uuid_url, json=[name]).json()
        if len(res) == 0:
            self.cache[name] = None
        else:
            res = requests.get(self.uuid_to_texture_url.format(res[0]["id"])).json()
            self.cache[name] = res["properties"][0]

        return self.cache[name]

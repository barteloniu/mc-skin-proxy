from select import select
from io import BytesIO
from enum import Enum
import zlib
from .utils import Varint
from .texture_manager import TextureManager
from . import packet_handler


class PacketReader():

    texture_manager = TextureManager()

    def __init__(self, source, dest):
        self.buffer = BytesIO()
        self.playing = False
        self.compression = None
        self.length = -1
        self.source = source
        self.dest = dest

    def decompress(self):
        self.buffer.seek(0)
        if not self.compression is None:
            data_length = Varint.read(self.buffer)
            if data_length != 0:
                # the packet is compressed
                uncompressed_data = BytesIO(zlib.decompress(self.buffer.read()))
            else:
                # compression enabled, but the packet length doesn't meet the threshold
                uncompressed_data = self.buffer
        else:
            # compression not enabled
            uncompressed_data = self.buffer

        packet_handler.parse(uncompressed_data, self)

    def send(self):
        # wait for the socket to be writable
        select([], [self.dest], [])
        Varint.write(self.length, self.dest)
        self.buffer.seek(0)
        self.dest.send(self.buffer.read())
        # prepare for the next packet
        self.length = -1
        self.buffer = BytesIO()

    def recv(self):
        if self.length == -1:
            # new packet
            self.length = Varint.read(self.source)
        else:
            if self.buffer.tell() < self.length:
                self.buffer.write(self.source.recv(self.length - self.buffer.tell()))
                if self.buffer.tell() == self.length:
                    self.decompress()

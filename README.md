# mc-skin-proxy

A proxy server for Minecraft to enable skins in offline mode

## Useful resources
- [Minecraft protocol documentation](https://wiki.vg/Protocol)
- [pyCraft - a third party client written in python](https://github.com/ammaraskar/pyCraft)
